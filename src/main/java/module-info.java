module net.liceo.navegador {
    requires javafx.controls;
    requires javafx.fxml;


    opens net.liceo.navegador to javafx.fxml;
    exports net.liceo.navegador;
}