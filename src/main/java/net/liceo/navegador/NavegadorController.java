package net.liceo.navegador;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class NavegadorController {
    public TextField filenameTF;
    public Label pathL;
    public Label lastModL;
    public VBox vBoxListFiles;
    public Label feedbackL;
    ListView listFilesLV = null;
    private String currentAbsPAth = "";
    List<String> listFiles = null;

    private static String getTime(String pathFile) {
        try {
            FileTime fileTime = Files.getLastModifiedTime(Paths.get(pathFile));
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
            return dateFormat.format(fileTime.toMillis());
        }catch (IOException e) {
            return "";
        }
    }

    public void goPath(String path){
        listFiles = new ArrayList<>();

        File newFile = new File(path.trim());

        if (newFile.exists()) {
            currentAbsPAth = newFile.getAbsolutePath();
            if (newFile.isDirectory()) {
                try {
                    File[] arrayFiles = newFile.listFiles();
                    for (File f : arrayFiles) {
                        listFiles.add(f.getName());
                    }
                }catch (Exception e){
                    System.out.println(e.getMessage());

                }
            }
        }
        refreshInfo(newFile);
        fillListFiles(listFiles);
    }

    @FXML
    public void onGoButton(ActionEvent actionEvent) {
        if (filenameTF.getText().trim().isEmpty()){
            return;
        }
        goPath(filenameTF.getText());
    }

    private void refreshInfo(File file) {
        if (file.exists()) {
            pathL.setText(file.getAbsolutePath());
            lastModL.setText(getTime(file.getAbsolutePath()));
        } else {
            pathL.setText("---");
            lastModL.setText("---");
        }
        if (listFiles.isEmpty()){
            if (file.isFile()) {
                feedbackL.setText("Ficheiro regular");
                feedbackL.setTextFill(Color.DARKGRAY);
            } else {
                feedbackL.setText("Non se puido ler o directorio '" + file.getName() + "'.");
                feedbackL.setTextFill(Color.RED);
            }
        } else {
            feedbackL.setText("");
        }
    }
    @FXML
    public void onClickListFile(ActionEvent actionEvent){

    }

    private void fillListFiles(List<String> listFiles) {
        ObservableList<Node> childs = vBoxListFiles.getChildren();
        if (listFilesLV == null) {
            listFilesLV = new ListView();
            listFilesLV.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    try {
                        String selected = listFilesLV.getSelectionModel().getSelectedItem().toString();
                        String newAbsPath = currentAbsPAth + File.separator + selected;

                        filenameTF.setText(newAbsPath);
                        goPath(newAbsPath);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            });
            childs.add(listFilesLV);
        } else{
            listFilesLV.getItems().clear();
        }

        for (String fn:listFiles) {
            listFilesLV.getItems().add(fn);
        }


    }
}